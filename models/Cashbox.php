<?php

namespace app\models;

use app\models\base\CompanyRecord;

/**
 * This is the model class for table "cashbox".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $name
 * @property integer $is_default
 *
 * @property Company $company
 */
class Cashbox extends CompanyRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cashbox';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'name'], 'required'],
            [['company_id', 'is_default'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [
                ['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(),
                'targetAttribute' => ['company_id' => 'id'],
            ],
            ['name', 'unique', 'targetAttribute' => ['company_id', 'name']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'name' => 'Name',
            'is_default' => 'Is Default',
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->is_default) {
            Cashbox::updateAll(['is_default' => 0, 'company_id' => $this->company_id]);
        } else {
            if (!Cashbox::find()->where(['is_default' => 1, 'company_id' => $this->company_id])->exists()) {
                $this->is_default = 1;
            }
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
