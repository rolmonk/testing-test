<?php


namespace app\models\base;


use app\models\Company;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class CompanyRecord
 *
 * @package app\models\base
 *
 * @property Company $company
 * @property integer $company_id
 */
class CompanyRecord extends ActiveRecord
{
    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    /**
     * @param $condition
     *
     * @return \app\models\base\CompanyRecord|null
     * @throws \yii\base\InvalidConfigException
     */
    public static function findOne($condition)
    {
        return static::findByCondition($condition)
            ->andWhere([self::tableName() . '.company_id' => Yii::$app->user->company->id ?? null])
            ->one();
    }

    /**
     * @param $condition
     *
     * @return array|\app\models\base\CompanyRecord[]|static[]
     * @throws \yii\base\InvalidConfigException
     */
    public static function findAll($condition)
    {
        return static::findByCondition($condition)
            ->andWhere([self::tableName() . '.company_id' => Yii::$app->user->company->id ?? null])
            ->all();
    }

    public function beforeValidate()
    {
        if (!parent::beforeValidate()) {
            return false;
        }

        $this->company_id = Yii::$app->user->company->id;

        return true;
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->company_id = Yii::$app->user->company->id;

        return true;
    }

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}