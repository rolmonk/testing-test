<?php


namespace app\models\base;


use Yii;
use yii\db\ActiveQuery;

class CompanyQuery extends ActiveQuery
{
    public function prepare($builder)
    {
        //$this->andWhere([$this->modelClass::tableName() . '.company_id' => Yii::$app->user->company->id ?? null]);

        return parent::prepare($builder);
    }

    public function company($id = null)
    {
        return $this->andWhere([$this->modelClass::tableName() . '.company_id' => $id ?: Yii::$app->user->company->id]);
    }
}