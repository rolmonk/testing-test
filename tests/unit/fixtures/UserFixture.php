<?php

namespace tests\unit\fixtures;

use app\models\Company;
use app\models\User;
use yii\test\ActiveFixture;

class UserFixture extends ActiveFixture
{
    public $modelClass = User::class;
    public $depends = [
        CompanyFixture::class,
    ];
}