<?php


namespace tests\unit\fixtures;


use app\models\Company;
use yii\test\ActiveFixture;

class CompanyFixture extends ActiveFixture
{
    public $modelClass = Company::class;
}