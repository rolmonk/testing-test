<?php /** @noinspection ALL */

use Faker\Factory;

$faker = Factory::create();

return [
    [
        'username' => $faker->name,
        'company_id' => 1,
        'auth_key'=>Yii::$app->security->generateRandomString(),
        'password_hash'=>Yii::$app->security->generatePasswordHash($faker->password),
        'email'=>$faker->email,
        'created_at'=>$faker->unixTime,
        'updated_at'=>$faker->unixTime,
    ],
    [
        'username' => $faker->name,
        'company_id' => 2,
        'auth_key'=>Yii::$app->security->generateRandomString(),
        'password_hash'=>Yii::$app->security->generatePasswordHash($faker->password),
        'email'=>$faker->email,
        'created_at'=>$faker->unixTime,
        'updated_at'=>$faker->unixTime,
    ],
];