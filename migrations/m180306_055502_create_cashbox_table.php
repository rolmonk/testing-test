<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cashbox`.
 * Has foreign keys to the tables:
 *
 * - `company`
 */
class m180306_055502_create_cashbox_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cashbox', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'is_default' => $this->boolean()->unsigned()->notNull()->defaultValue(0),
        ]);


        // creates index for column `company_id`
        $this->createIndex(
            'idx-cashbox-is_default',
            'cashbox',
            'is_default'
        );

        // creates index for column `company_id`
        $this->createIndex(
            'idx-cashbox-company_id',
            'cashbox',
            'company_id'
        );

        // add foreign key for table `company`
        $this->addForeignKey(
            'fk-cashbox-company_id',
            'cashbox',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `company`
        $this->dropForeignKey(
            'fk-cashbox-company_id',
            'cashbox'
        );

        // drops index for column `company_id`
        $this->dropIndex(
            'idx-cashbox-company_id',
            'cashbox'
        );

        $this->dropTable('cashbox');
    }
}
