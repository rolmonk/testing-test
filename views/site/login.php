<?php

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

use app\models\User;
use yii\helpers\Html;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="list-group">
        <?php foreach (User::find()->each() as $user) {
            /** @var User $user */
            echo Html::a($user->username, ['', 'id' => $user->id], ['class' => 'list-group-item', 'data-method'=>'post']);
        } ?>
    </div>
</div>
