<?php

/** @var $this \yii\web\View */

use app\models\Cashbox;
use kartik\grid\ActionColumn;
use kartik\grid\EditableColumn;
use kartik\grid\GridView;
use kartik\grid\RadioColumn;

/** @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = 'Cashboxes';
?>

<?= $this->render('_form', ['model' => new Cashbox()]) ?>

    <hr>

<?= /** @noinspection PhpUnhandledExceptionInspection */
GridView::widget([
    'id' => 'cashboxes-grid',
    'dataProvider' => $dataProvider,
    'rowOptions' => function (Cashbox $model) {
        return $model->is_default ? ['class' => 'success'] : [];
    },
    'columns' => [
        [
            'class' => EditableColumn::className(),
            'attribute' => 'name',
            'editableOptions' => ['formOptions' => ['action' => '/cashboxes/update']],
        ],
        [
            'class' => RadioColumn::className(),
            'header' => 'Is Default',
            'radioOptions' => function (Cashbox $model) {
                return ['checked' => $model->is_default];
            },
        ],
        [
            'class' => ActionColumn::className(),
            'template' => '{delete}',
        ],
    ],
]) ?>

<?php
$js = <<<JS
$('#cashboxes-grid').on('grid.radiochecked', function(ev, key, val) {
    $.post('/cashboxes/set-default' + '?id=' + key);
});
JS;

$this->registerJs($js);
