<?php

/* @var \yii\web\View $this */

/* @var \common\models\Cashbox $model */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<?php $form = ActiveForm::begin([
    'layout' => 'inline',
    'action' => ['create'],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]) ?>

<?= $form->field($model, 'name') ?>

<?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>

<?php $form->end() ?>
