<?php


namespace app\controllers;

use app\models\Cashbox;
use kartik\grid\EditableColumnAction;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class CashboxesController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'update' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => Cashbox::className(),
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Cashbox::find()->company(),
            'sort' => false,
            'pagination' => false,
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionCreate()
    {
        $model = new Cashbox();

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($model);
            }

            $model->save();
        }

        return $this->redirect(['cashboxes/index']);
    }

    /**
     * @param $id
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSetDefault($id)
    {
        $model = $this->findModel($id);
        Cashbox::updateAll(['is_default' => 0], ['company_id' => Yii::$app->user->company->id]);
        $model->updateAttributes(['is_default' => 1]);
    }

    /**
     * @param $id
     *
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['cashboxes/index']);
    }

    /**
     * @param $id
     *
     * @return \app\models\base\CompanyRecord|Cashbox
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    protected function findModel($id)
    {
        if (!$model = Cashbox::findOne($id)) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}