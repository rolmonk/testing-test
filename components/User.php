<?php


namespace app\components;

/**
 * Class User
 *
 * @package common\components
 *
 * @property \app\models\User $identity
 * @property \app\models\Company $company
 */
class User extends \yii\web\User
{
    public function getCompany()
    {
        return $this->identity->company ?? null;
    }
}